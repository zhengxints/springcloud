package com.zx.cloud.service.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zx.cloud.service.domain.UserService;
import com.zx.cloud.service.model.User;

@RestController
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value="/user",method=RequestMethod.GET)
	public @ResponseBody List<User> readUserInfo(){
		List<User> ls=userService.searchAll();		
		return ls;
	}
}
