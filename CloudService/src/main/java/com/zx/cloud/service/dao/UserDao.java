package com.zx.cloud.service.dao;

import java.util.List;

import com.zx.cloud.service.model.User;

public interface UserDao {

    List<User> findAll();
}
