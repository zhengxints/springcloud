package com.zx.cloud.service;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
@SpringBootApplication
@EnableEurekaClient
public class CloudServiceApplication {

    @ResponseBody
    @RequestMapping(value = "/")
    String home() {
        return new Date() + " Hello World! This is Cloud Spring War";
    }

    public static void main(String[] args)
        throws Exception {
        // SpringApplication.run(CloudController.class, args);
        SpringApplication.run(CloudServiceApplication.class, new String[]{"--config.profile=test"});//," --server.port=8081"
//        SpringApplication.run(CloudServiceApplication.class, "--config.profile=test"," --server.port=8082") ;
//        SpringApplication.run(CloudConfigServerApplication.class, "--server.port=8081");
    }
}
