本系统归纳自：http://www.cnblogs.com/skyblog/p/5129603.html  

CloudConfigRepo  配置文件存放的文件夹
    
    分布式配置管理应该是分布式系统和微服务应用的第一步。
    想象一下如果你有几十个服务或应用需要配置，而且每个服务还分为开发、测试、生产等不同维度的配置，那工作量是相当大的，而且还容易出错。
    如果能把各个应用的配置信息集中管理起来，使用一套机制或系统来管理，那么将极大的提高系统开发的生产效率，同时也会提高系统开发环境和生产环境运行的一致性。
    在传统开发中我们往往需要自己开发“配置管理服务器”，你可以使用redis、ldap、zookeeper、db等来存放统一配置信息，然后开发一个管理界面来进行管理。传统的做法没什么问题，和spring cloud所提供的配置管理方案相比，就是前者需要自己开发，而后者直接简单使用现成的组件即可。当然还有很重要的一点，spring 配置管理模块由于是spring boot核心来实现的，因此做了大量的工作，可以把一些启动参数进行外部配置，这在传统的方案中是很难办到的，因为涉及到要改写第三方组件的问题，难度很大。比如web应用的绑定端口，传统应用只能在tomcat配置文件里改，而spring cloud却可以放到远程，类似的还有数据库连接、安全框架配置等。
    要使用spring cloud分布式配置文件总体上分为3个大的步骤，首选你需要创建存放配置文件的仓库，然后创建一个配置文件服务器，该服务器将配置文件信息转化为rest接口数据，然后创建一个应用服务，该服务演示使用分布式配置文件信息。
    
#1）创建配置文件存放仓库CloudConfigRepo
	Spring cloud使用git或svn存放配置文件，默认情况下使用git，因此你需要安装git私服或者直接使用互联网上的github或者git.oschina，这里推荐使用git.oschina。本文示例使用的是git.oschina，创建好git工程后，也就是文章开头所提到的工程，在此工程再创建一个文件夹CloudConfigRepo来存放配置文件。然后创建两个配置文件分别对应开发环境和测试环境所需要的配置信息（配置信息提供了数据库连接参数）：
    cloud-config-dev.properties
    cloud-config-test.properties
    
#2）创建spring cloud配置服务器CloudConfigServer
	配置文件仓库创建好了后，就需要创建配置管理服务器，如前所述该服务器只是将配置文件转换为rest接口服务，不做其它用途。这个服务器的功能也是spring cloud提供的，所以我们只需要引入相关jar包，稍微设置一下即可。
	CloudConfigServerApplication用@EnableConfigServer激活该应用为配置文件服务器即可。如此以来该应用启动后就会完成前面提到的功能，即：读取远程配置文件，转换为rest接口服务。
	需要配置远程配置文件读取路径，在application.properties中：
	server.port=8888
	spring.cloud.config.server.git.uri=https://git.oschina.net/zhengxints/springcloud.git
	spring.cloud.config.server.git.searchPaths=CloudConfigRepo
	eureka.client.serviceUrl.defaultZone=http\://localhost\:8761/eureka/,http\://localhost\:8762/eureka/
	spring.application.name=CloudConfigServer
	其中server.port是配置当前web应用绑定8888端口，git.uri指定配置文件所在的git工程路径，searchPaths表示将搜索该文件夹下的配置文件（我们的配置文件放在工程的cloud-config-repo文件夹下）。
	pom添加依赖：
	<dependency>
		<groupId>org.springframework.cloud</groupId>
		<artifactId>spring-cloud-config-server</artifactId>
	</dependency>
	配置文件服务器就建立好了，可以直接启动了，服务端口是8888，应用只需要绑定改服务器的uri和端口号就可以拿到配置信息了。
	
#3）  创建一个服务使用该远程配置
	设置配置管理服务器地址，该配置设置在：
	bootstrap.properties
	spring.cloud.config.uri=http://127.0.0.1:${config.port:8888}
	spring.cloud.config.name=cloud-config
	spring.cloud.config.profile=${config.profile:dev}
	其中config.uri指定远程加载配置信息的地址，就是前面我们刚建立的配置管理服务器的地址，绑定端口8888，其中config.port:8888，表示如果在命令行提供了config.port参数，我们就用这个端口，否则就用8888端口。
	config.name表示配置文件名称，查看我们前面创建配置文件，是这个名称： cloud-config-dev.properties
	可以分成两部分: {application}- {profile}.properties
	所以我们配置config.name为cloud-config，config.profile为dev，其中dev表示开发配置文件，配置文件仓库里还有一个测试环境的配置文件，切换该配置文件只需要将dev改为test即可，当然这个参数也可以由启动时命令行传入，如：
	java -jar cloud-simple-service-1.0.0.jar --config.profile =test 
	or SpringApplication.run(CloudServiceApplication.class, "--config.profile =test");
	此时应用就会加载测试环境下的配置信息。

#服务注册及发现（eureka注册服务器）
	所有的服务端及访问服务的客户端都需要连接到注册管理器（eureka服务器）。服务在启动时会自动注册自己到eureka服务器，每一个服务都有一个名字，这个名字会被注册到eureka服务器。使用服务的一方只需要使用该名字加上方法名就可以调用到服务。
	Spring cloud的服务注册及发现，不仅仅只有eureka，还支持Zookeeper和Consul。
	1）首选需要建立eureka服务器
	只需要使用 @EnableEurekaServer注解就可以让应用变为Eureka服务器。EurekaServer是Netflix公司的开源项目，也是可以单独下载使用的。
	server.port=8761
	eureka.instance.hostname=localhost
	eureka.client.registerWithEureka=false
	eureka.client.fetchRegistry=false
	eureka.client.serviceUrl.defaultZone=http://${eureka.instance.hostname}:${server.port}/eureka/
	其中server.port配置eureka服务器端口号。Eureka的配置属性都在开源项目spring-cloud-netflix-master中定义（spring boot连文档都没有，只能看源码了），在这个项目中有两个类EurekaInstanceConfigBean 和EurekaClientConfigBean， 分别含有eureka.instance和eureka.client相关属性的解释和定义。从中可以看到，registerWithEureka表示是 否注册自身到eureka服务器，因为当前这个应用就是eureka服务器，没必要注册自身，所以这里是false。fetchRegistry表示是否 从eureka服务器获取注册信息，同上，这里不需要。defaultZone就比较重要了，是设置eureka服务器所在的地址，查询服务和注册服务都 需要依赖这个地址。
	2）让服务使用eureka服务器
	让服务使用eureka服务器，只需 添加@EnableDiscoveryClient注解就可以了。回到我们在上篇文章中实现的cloud-simple-service微服务应用。在 main方法所在的Application类中，添加@EnableDiscoveryClient注解。然后在配置文件中添加：
	eureka.client.serviceUrl.defaultZone=http\://localhost\:8761/eureka/
	spring.application.name=cloud-simple-service
	
#启动：
	启动eurekaServer,ConfigServer,Server(1,2),WEB
	
mvn versions:set -DnewVersion=1.0.0-SNAPSHOT	升级父项目以及所有子项目的版本
mvn versions:update-child-modules		修改所有被依赖的版本
mvn release:prepare		切换为正式版本
mvn release:perform		要发布正式版本时
